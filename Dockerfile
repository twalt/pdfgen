﻿FROM mcr.microsoft.com/dotnet/aspnet:5.0-focal AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:5.0-focal AS build
WORKDIR /src
COPY ["PdfGen/PdfGen.csproj", "PdfGen/"]
RUN dotnet restore "PdfGen/PdfGen.csproj"
COPY . .
WORKDIR "/src/PdfGen"
RUN dotnet build "PdfGen.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "PdfGen.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app

RUN apt-get update \
    && apt-get install -y software-properties-common \
    && add-apt-repository ppa:ubuntu-mozilla-daily/ppa \
    && apt-get update \
    && apt-get install -y firefox-trunk=94.0~a1~hg20210918r592422-0ubuntu0.20.04.1~umd1

COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "PdfGen.dll"]
