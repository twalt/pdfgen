using System.IO;
using System.Threading.Tasks;

namespace PdfGen
{
    public interface IBrowserService
    {
        public Task<string> PrintPage(string url);
        Task<string> PrintHtmlAndCss(string html, string css = "");
    }
}