using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using PuppeteerSharp;
using PuppeteerSharp.Media;

namespace PdfGen
{
    class BrowserService : IBrowserService, IDisposable
    {
        private readonly SemaphoreSlim _tabsSemaphore;
        private readonly ILogger<BrowserService> _logger;
        private Browser _browser;
        private static readonly object RestartLock = new();
        
        public BrowserService(SemaphoreSlim tabsSemaphore, ILogger<BrowserService> logger)
        {
            _tabsSemaphore = tabsSemaphore;
            _logger = logger;
            _browser = Puppeteer.LaunchAsync(new LaunchOptions()
            {
                ExecutablePath = "/usr/bin/firefox-trunk",
                Headless = true,
            }).Result;
        }
        
        public async Task<string> PrintPage(string url)
        {
            if (_browser.IsClosed || !_browser.IsConnected)
            {
                _logger.LogError("Browser is not running");
                throw new InvalidOperationException("Browser is not running");
            }
            try
            {
                await _tabsSemaphore.WaitAsync();
                var page = await _browser.NewPageAsync();
                await page.GoToAsync(url);
            
                var pdf = await page.PdfDataAsync(new PdfOptions
                {
                    PrintBackground = true
                });
                _logger.LogDebug("Generate PDF");
                
                await page.CloseAsync();
                _tabsSemaphore.Release();
                return Convert.ToBase64String(pdf);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Creating Pdf failed! Restarting Browser. Error: {Error}", e.Message);
                await RestartBrowser();
                throw new InvalidOperationException("Browser Error");
            }
        }

        public async Task<string> PrintHtmlAndCss(string html, string css = "")
        {
            if (_browser.IsClosed || !_browser.IsConnected)
            {
                _logger.LogError("Browser is not running");
                throw new InvalidOperationException("Browser is not running");
            }
            try
            {
                await _tabsSemaphore.WaitAsync();
                var page = await _browser.NewPageAsync();
                await page.SetContentAsync(html);
                if (!string.IsNullOrWhiteSpace(css))
                {
                    await page.AddStyleTagAsync(new AddTagOptions()
                    {
                        Content = css
                    });
                }

                var pdf = await page.PdfDataAsync(new PdfOptions
                {
                    PrintBackground = true
                });
                _logger.LogDebug("Generate PDF");
                
                await page.CloseAsync();
                _tabsSemaphore.Release();
                return Convert.ToBase64String(pdf);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Creating Pdf failed! Restarting Browser. Error: {Error}", e.Message);
                await RestartBrowser();
                throw new InvalidOperationException("Browser Error");
            }
        }

        private async Task RestartBrowser()
        {
            if (Monitor.TryEnter(RestartLock))
            {
                _logger.LogInformation("Entered Lock to restart Browser");
                try
                {
                    if (!_browser.IsClosed)
                    {
                        _logger.LogInformation("Browser is not closed. Closing...");
                        await _browser.CloseAsync();
                        _logger.LogInformation("Browser closed");
                    }
                    
                    _logger.LogInformation("Starting Browser");
                    _browser = await Puppeteer.LaunchAsync(new LaunchOptions()
                    {
                        ExecutablePath = "/usr/bin/firefox-trunk",
                        Headless = true,
                    });
                    _logger.LogInformation("Browser started again");
                }
                finally
                {
                    Monitor.Exit(RestartLock);
                    _logger.LogInformation("Lock exited");
                }
            }
        }

        public void Dispose()
        {
            _browser?.Dispose();
        }
    }
}