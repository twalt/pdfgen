using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace PdfGen.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BrowserController : ControllerBase
    {
        private readonly ILogger<BrowserController> _logger;
        private readonly IBrowserService _browserService;

        public BrowserController(ILogger<BrowserController> logger, IBrowserService browserService)
        {
            _logger = logger;
            _browserService = browserService;
        }

        [HttpPost("HtmlAndCss")]
        public async Task<IActionResult> PostHtmlAndCssAsync([FromBody] HtmlAndCssRequest htmlAndCssRequest)
        {
            if (!string.IsNullOrWhiteSpace(htmlAndCssRequest.Base64Css))
            {
                _logger.LogDebug("Request received without Html");
                return BadRequest("html is needed");
            }

            var html = Encoding.UTF8.GetString(Convert.FromBase64String(htmlAndCssRequest.Base64Html));
            var css = string.Empty;
            if (!string.IsNullOrWhiteSpace(htmlAndCssRequest.Base64Css))
            {
                css = Encoding.UTF8.GetString(Convert.FromBase64String(htmlAndCssRequest.Base64Css));
            }

            var pdf = await _browserService.PrintHtmlAndCss(html, css);
            return Ok(pdf);
        }
    }
}