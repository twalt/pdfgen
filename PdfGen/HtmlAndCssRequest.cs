using System.Text.Json.Serialization;

namespace PdfGen
{
    public class HtmlAndCssRequest
    {
        [JsonPropertyName("html")]
        public string Base64Html { get; set; }
        [JsonPropertyName("css")]
        public string Base64Css { get; set; }
    }
}